package abc.practice3_5;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Onclick(View v)
    {

        EditText edit = (EditText) findViewById(R.id.editText1);
        String input = edit.getText().toString();

        switch(v.getId())
        {
            case R.id.save :
            {

                if(input.length() <= 50)
                {
                    edit.setText(input + "가 SAVE되었습니다.");
                }

                else
                {
                    edit.setText("저장할 수 없습니다.");
                }
                break;
            }

            case R.id.cancel :
            {
                edit.setText("");
                break;
            }

            case R.id.exit :
            {
                edit.setText("exit!");
                break;
            }

        }
    }
}
